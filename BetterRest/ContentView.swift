//
//  ContentView.swift
//  BetterRest
//
//  Created by Hariharan S on 08/05/24.
//

import CoreML
import SwiftUI

struct ContentView: View {
    @State private var wakeUp = ContentView.defaultWakeTime
    @State private var sleepAmount = 8.0
    @State private var coffeeAmount = 1
    
    @State private var alertTitle = ""
    @State private var alertMessage = ""
    @State private var showingAlert = false
    
    static var defaultWakeTime: Date {
        var components = DateComponents()
        components.hour = 7
        components.minute = 0
        return Calendar.current.date(from: components) ?? .now
    }
    
    var body: some View {
        NavigationStack {
            Form {
                Section("When do you want to wake up?") {
                    DatePicker(
                        "Update your wakeUp time",
                        selection: self.$wakeUp,
                        displayedComponents: .hourAndMinute
                    )
                }
                
                Section("Desired amount of sleep") {
                    Stepper(
                        "\(self.sleepAmount.formatted()) hours",
                        value: self.$sleepAmount,
                        in: 4...12, step: 0.25
                    )
                }
                
                Section("Daily coffee intake") {
                    Picker(
                        "^[\(self.coffeeAmount) cup](inflect: true)",
                        selection: self.$coffeeAmount
                    ) {
                        ForEach(0..<11) {
                            Text("\($0)")
                        }
                    }
                }
                
                HStack {
                    Text("Your recommended bedtime is ")
                        .font(.headline)
                    Text(self.calculateBedtime(), style: .time)
                        .font(.subheadline)
                        .fontWeight(.thin)
                        .fontDesign(.monospaced)
                    Spacer()
                }
                .padding(.vertical, 10)
            }
            .navigationTitle("Better Rest")
            .alert(self.alertTitle, isPresented: $showingAlert) {
                Button("OK") { }
            } message: {
                Text(self.alertMessage)
            }
        }
    }
}

// MARK: - Private Method

private extension ContentView {
    func calculateBedtime() -> Date {
        do {
            let config = MLModelConfiguration()
            let model = try SleepCalculator(configuration: config)

            let components = Calendar.current.dateComponents(
                [.hour, .minute], 
                from: self.wakeUp
            )
            let hour = (components.hour ?? 0) * 60 * 60
            let minute = (components.minute ?? 0) * 60
            
            let prediction = try model.prediction(
                wake: Double(hour + minute),
                estimatedSleep: self.sleepAmount,
                coffee: Double(self.coffeeAmount)
            )
            
            let sleepTime = self.wakeUp - prediction.actualSleep
            return sleepTime
        } catch {
            self.alertTitle = "Error"
            self.alertMessage = "Sorry, there was a problem calculating your bedtime."
            self.showingAlert = true
            return .now
        }
    }
}

#Preview {
    ContentView()
}
