//
//  BetterRestApp.swift
//  BetterRest
//
//  Created by Hariharan S on 08/05/24.
//

import SwiftUI

@main
struct BetterRestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
